import './App.css';
import Jobs from "./jobs.json";
import JobBriefList from "./components/JobBriefList"
import JobBrief from "./components/JobBrief"
import Demo from "./components/Demo.js";
const postDetails=Jobs.map((postDetail)=><button> <JobBriefList key={postDetail.id} name={postDetail.name} location={postDetail.location.city} description={postDetail.description} salary={postDetail.salary} logo={postDetail.logo}/></button>)

function App() {
  return (
    <div className="App ">
      <header className="App-header">
    {/*  <Demo/> */}
       
        <div className="split left">
        {postDetails}
        </div>
        <div className="split right">
        <JobBrief name={Jobs[1].name} salary={Jobs[1].salary} location={Jobs[1].location.city} description={Jobs[1].description} />
     
        </div>
  
      </header>
    </div>
  );
}

export default App;
