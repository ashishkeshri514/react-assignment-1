import React, {Component} from 'react';
import './JobBriefList.css';



class Post extends Component{
    render(){return(
<div className="card">
            <div className="container">
           <h1> <b>{this.props.name}</b></h1>
           <h2> <b>{this.props.location}</b></h2>
           <img src={this.props.logo}  alt="profile"/><br></br>
           <i>{this.props.description}</i><br></br>
           <i>Salary: {this.props.salary}</i><br></br>
            <button className="apply_button_css">Apply</button>  <button className="not_interested_button_css"> Not Interested </button> 
 </div>
 </div>
        )
    }
}

export  default Post;